// This example adds a red rectangle to a map.
let rectangle;
let map;
let infoWindow;
function initMap() {
    map = new google.maps.Map(document.getElementById("map"), {
        center: { lat: 24.6921856, lng: 46.7378258 },
        zoom: 9,
    });
    const default_bounds = {
        north: 24.8540561,
        south: 24.6081772,
        east: 46.9633807,
        west: 46.5400476,
    };
    let bounds=$('#bounds').val()==''?default_bounds:JSON.parse($('#bounds').val());
    // Define the rectangle and set its editable property to true.
    rectangle = new google.maps.Rectangle({
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.35,
        bounds: bounds,
        editable: true,
        draggable: true,
    });
    rectangle.setMap(map);
    // Add an event listener on the rectangle.
    rectangle.addListener("bounds_changed", showNewRect);

    google.maps.event.addListener(rectangle, "bounds_changed",function(e){
        $('#bounds').val(JSON.stringify(rectangle.getBounds()));
    });
    // Define an info window on the map.
    infoWindow = new google.maps.InfoWindow();
}

/** Show the new coordinates for the rectangle in an info window. */
function showNewRect() {
    const ne = rectangle.getBounds().getNorthEast();
    const sw = rectangle.getBounds().getSouthWest();
    const contentString =
        "<b>Rectangle moved.</b><br>" +
        "New north-east corner: " +
        ne.lat() +
        ", " +
        ne.lng() +
        "<br>" +
        "New south-west corner: " +
        sw.lat() +
        ", " +
        sw.lng();
    // Set the info window's content and position.
    infoWindow.setContent(contentString);
    infoWindow.setPosition(ne);
    infoWindow.open(map);
}
