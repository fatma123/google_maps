<li>
    <a class="check_active" href="{{route('admin.home')}}">
        <i class="material-icons">home</i>
        <span>الصفحه الرئيسه والاحصائيات</span>
    </a>

</li>

<li class="header">عام</li>

<li>
    <a href="javascript:void(0);" class="menu-toggle">
        <i class="material-icons">add_to_photos</i>

        <span>المدن</span>
    </a>
    <ul class="ml-menu">
        <li>
            <a class="check_active" href="{{route('admin.regions.index')}}">

                <span>عرض المدن </span>
            </a>
        </li>

        <li>
            <a class="check_active" href="{{route('admin.regions.create')}}">

                <span>اضافه جديد</span>
            </a>
        </li>
    </ul>
</li>
