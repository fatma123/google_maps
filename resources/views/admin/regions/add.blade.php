@extends('admin.layout.app')

@section('title')
    اضافة مدينة جديدة
@stop
@section('content')

    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>اضافة مدينة جديدة</h2>
                    <ul class="header-dropdown m-r--5">
                        <a href="{{route('admin.regions.index')}}">
                            <button class="btn btn-danger1">كل المدن</button>
                        </a>
                    </ul>
                </div>
                <div class="body">
                    {!!Form::open( ['route' => 'admin.regions.store' ,'class'=>'form phone_validate', 'enctype'=>"multipart/form-data",'files' => true]) !!}
                    @include('admin.regions.form')
                    {!!Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- #END# Basic Validation -->
@endsection

@push('scripts')
    <script src="{{ asset('admin/map.js') }}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsT140mx0UuES7ZwcfY28HuTUrTnDhxww&callback=initMap&libraries=&v=weekly"></script>
@endpush
