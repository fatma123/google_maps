@extends('admin.layout.app')

@section('title')
    تعديل المدينة{{$item->name}}
@stop
@section('content')

    <!-- Basic Validation -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>  تعديل المدينة{{$item->name}}</h2>
                    <ul class="header-dropdown m-r--5">
                        <a href="{{route('admin.regions.index')}}">
                            <button class="btn btn-danger">حميع المدن</button>
                        </a>
                    </ul>
                </div>
                <div class="body">
                    {!!Form::model($item , ['route' => ['admin.regions.update' , $item->id] , 'method' => 'PATCH','enctype'=>"multipart/form-data",'id'=>'EditForm','files' => true]) !!}
                    @include('admin.regions.form')
                    {!!Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('admin/map.js') }}"></script>
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCsT140mx0UuES7ZwcfY28HuTUrTnDhxww&callback=initMap&libraries=&v=weekly"></script>

@endpush
