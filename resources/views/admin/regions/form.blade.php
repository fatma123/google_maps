@include('admin.common.alert')
@include('admin.common.errors')


<div class="form-group form-float">
    <label class="form-label">الإسم باللغة الــعربية  </label>
    <div class="form-line">
        {!! Form::text("name",null,['class'=>'form-control','placeholder'=>' name'])!!}
    </div>
</div>

<div class="form-group form-float">
    <label class="form-label">موقعها</label>
    <input hidden name="lat"id="lat" value="">
    <input hidden name="lng"  id="lng" value="">
    <input hidden name="bounds"  id="bounds" value="{{isset($item)?$item->bounds:null}}">
    <div id="map" style="height: 400px"></div>
</div>



<button class="btn btn-primary waves-effect" type="submit">حفظ</button>
