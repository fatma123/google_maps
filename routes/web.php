<?php

use Illuminate\Support\Facades\Route;
use \Illuminate\Support\Facades\Mail;

Route::get('/', function () {
    return view('admin.auth.login');
});


Route::group(['prefix' => 'dashboard', 'as' => 'admin.','middleware'=>'auth'], function () {
        Route::get('/','IndexController@index')->name('home');
        Route::resource('regions','RegionController');
});

Route::get('send-mail', function () {
    Mail::send(new \App\Mail\sendEmailMailable());
});

Route::get('/contact', 'HomeController@contact');
Route::post('/post-contact', 'HomeController@postContact');

require __DIR__ . '/auth.php';
